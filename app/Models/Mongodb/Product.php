<?php

namespace App\Models\Mongodb;

use \Jenssegers\Mongodb\Eloquent\Model;

class Product extends Model
{
    protected $connection = 'mongodb';
    protected $table = 'products';
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    protected $fillable = [
        "name",
        "description",
        "price",
        "category",
        'created_at',
        "updated_at",
        "deleted_at",
    ];
}
