<?php

namespace App\Models\Mysql;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $connection = 'mysql';
    protected $table = 'order';

    protected $fillable = [
        "order_number",
        "checkout_date",
        "product_data",
        "total_product",
        "total_quantity",
        "total_price",
        "is_paid",
        "is_approve",
        "is_payment_uploaded",
        "payment_uploaded_file",
        "payment_uploaded_file_at",
        "notes",
        "is_deleted",
        "created_by",
        "created_at",
        "updated_by",
        "updated_at",
        "deleted_by",
        "deleted_at",
    ];
}
