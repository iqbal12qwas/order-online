<?php

namespace App\Models\Mysql;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    protected $connection = 'mysql';
    protected $table = 'cart';

    protected $fillable = [
        "data_product",
        "total_product",
        "total_quantity",
        "total_price",
        "is_selected",
        "created_at",
        "updated_at",
    ];
}
