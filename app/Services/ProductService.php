<?php

namespace App\Services;

use App\Repositories\Mongo\ProductRepository;
use App\Http\Resources\ListProductCollection;
use App\Traits\AdminTrait;
use Carbon\Carbon;

class ProductService
{
    use AdminTrait;

    protected $productRepo;

    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepo = $productRepository;
    }

    public function createProduct($request)
    {
        try {
            $dataProduct = [
                "name" => $request['name'],
                "description" => $request['description'],
                "price" => $request['price'],
                "category" => $request['category'],
                "created_at" => Carbon::now()->toDateTimeString(),
                "updated_at" => Carbon::now()->toDateTimeString(),
            ];
            $this->productRepo->store($dataProduct);

            $resp['data'] = $dataProduct;

            return  $this->returnJsonSuccess($resp);
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function getDetail($id)
    {
        try {
            $resp['data'] = $this->productRepo->getById($id);
            return  $this->returnJsonSuccess($resp);
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function getAll()
    {
        try {
            $resp['data'] = $this->productRepo->getAll();
            return  $this->returnJsonSuccess($resp);
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function getPaginate($request)
    {
        try {
            $datas = $this->productRepo->getPeginate($request->per_page);

            $result = new ListProductCollection($datas);
            return $this->returnJsonSuccess($result->toArray($request));
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }
}
