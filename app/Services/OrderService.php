<?php

namespace App\Services;

use App\Repositories\Mysql\OrderRepository;
use App\Repositories\Mysql\CartRepository;
use App\Http\Resources\ListOrderCollection;
use App\Jobs\CreateOrder;
use App\Traits\AdminTrait;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class OrderService
{
    use AdminTrait;

    protected $orderRepo, $cartRepo;

    public function __construct(OrderRepository $orderRepository, CartRepository $cartRepository)
    {
        $this->orderRepo = $orderRepository;
        $this->cartRepo = $cartRepository;
    }

    public function createOrder()
    {
        try {
            $dataCartSelected = $this->cartRepo->getSelected();
            $totalCheckout = count($dataCartSelected);
            $productData = [];
            if ($totalCheckout > 0) {
                foreach ($dataCartSelected as $key => $value) {
                    $product =  json_decode($value['data_product'], true);
                    foreach ($product as $key2 => $value2) {
                        array_push($productData, $value2);
                    }
                }
                CreateOrder::dispatch($this->createOrderNumber())->onQueue('create_order');
            } else {
                throw new \Exception("Data Cart Not Selected!");
            }
            return  $this->returnJsonSuccess();
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function updatePaid($request)
    {
        try {

            $data = [
                "is_paid" => 1,
            ];

            $resp['data'] = $data;

            DB::beginTransaction();
            $this->orderRepo->update($request['ids'], $data);
            DB::commit();
            return  $this->returnJsonSuccess($resp);
        } catch (\Exception $e) {
            DB::rollback();
            throw new \Exception($e->getMessage());
        }
    }

    public function uploadPaymentOrder($request)
    {
        try {
            // Upload
            $resultUpload = $request->file('file')->store('public');
            if (!$resultUpload) {
                throw new \Exception("Upload file failed");
            }

            $data = [
                "is_payment_uploaded" => 1,
                "payment_uploaded_file" => $resultUpload,
                "payment_uploaded_file_at" =>  Carbon::now()->toDateTimeString(),
            ];

            $resp['data'] = $data;

            DB::beginTransaction();
            $this->orderRepo->update([$request['id']], $data);
            DB::commit();
            return  $this->returnJsonSuccess($resp);
        } catch (\Exception $e) {
            DB::rollback();
            throw new \Exception($e->getMessage());
        }
    }

    public function updateApprove($request)
    {
        try {
            // Validation Input
            $statusValid = array(0, 1, 9);
            if (!in_array($request['is_approve'], $statusValid)) {
                throw new \Exception("Approve Invalid");
            }

            $data = [
                "is_approve" => $request['is_approve'],
            ];

            $resp['data'] = $data;

            DB::beginTransaction();
            $this->orderRepo->update($request['ids'], $data);
            DB::commit();
            return  $this->returnJsonSuccess($resp);
        } catch (\Exception $e) {
            DB::rollback();
            throw new \Exception($e->getMessage());
        }
    }

    public function getAll()
    {
        try {
            $resp['data'] = $this->orderRepo->getAll();
            return  $this->returnJsonSuccess($resp);
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function getPaginate($request)
    {
        try {
            $datas = $this->orderRepo->getPeginate($request->per_page);

            $result = new ListOrderCollection($datas);
            return $this->returnJsonSuccess($result->toArray($request));
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function getSummaryOrder()
    {
        try {
            $summmaryNotPaid = $this->orderRepo->getSummaryNotPaid();
            $summmaryPaid = $this->orderRepo->getSummaryPaid();
            $summmaryPaymentUploaded = $this->orderRepo->getSummaryPaymentUploaded();
            $summmaryApproved = $this->orderRepo->getSummaryApproved();
            $summmaryRejected = $this->orderRepo->getSummaryRejected();

            $resp['data'] = [
                'not_paid' => $summmaryNotPaid,
                'paid' => $summmaryPaid,
                'payment_uploaded' => $summmaryPaymentUploaded,
                'approved' => $summmaryApproved,
                'rejected' => $summmaryRejected
            ];
            return  $this->returnJsonSuccess($resp);
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    private function createOrderNumber()
    {
        $timeStamp = strtotime(Carbon::now()->toDateTimeString());
        $orderNumber = "ON/" . $timeStamp . "/00001";
        return $orderNumber;
    }
}
