<?php

namespace App\Services;

use App\Repositories\Mysql\CartRepository;
use App\Repositories\Mongo\ProductRepository;
use App\Traits\AdminTrait;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class CartService
{
    use AdminTrait;

    protected $cartRepo, $productRepo;

    public function __construct(CartRepository $cartRepository, ProductRepository $productRepository)
    {
        $this->cartRepo = $cartRepository;
        $this->productRepo = $productRepository;
    }

    public function createCart($request)
    {
        try {
            $post = $request->all();
            $totalPrice = 0;
            $quantity = 0;
            $dataProducts = [];
            foreach ($post['data'] as $key => $value) {
                $dataProduct = $this->productRepo->getById($value['_id']);
                $totalPrice += $dataProduct->price * $value['quantity'];
                $quantity +=  $value['quantity'];
                array_push($dataProducts, $dataProduct);
            }
            $dataCart = [
                "data_product" => json_encode($dataProducts),
                "total_product" => count($post['data']),
                "total_quantity" => $quantity,
                "total_price" => $totalPrice,
                "is_selected" => 0,
                "created_at" => Carbon::now()->toDateTimeString(),
                "updated_at" => Carbon::now()->toDateTimeString(),
            ];
            $this->cartRepo->store($dataCart);

            $resp['data'] = $dataCart;

            return  $this->returnJsonSuccess($resp);
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function selectCartByIds($request)
    {
        try {
            DB::beginTransaction();
            $dataCart = [
                "is_selected" => 1,
            ];
            $this->cartRepo->update($request['ids'], $dataCart);
            DB::commit();
            return  $this->returnJsonSuccess();
        } catch (\Exception $e) {
            DB::rollback();
            throw new \Exception($e->getMessage());
        }
    }

    public function deleteCartByIds($request)
    {
        try {
            DB::beginTransaction();
            foreach ($request['id'] as $key => $value) {
                $this->cartRepo->delete($value);
            }
            DB::commit();
            return  $this->returnJsonSuccess();
        } catch (\Exception $e) {
            DB::rollback();
            throw new \Exception($e->getMessage());
        }
    }
}
