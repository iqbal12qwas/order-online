<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;
use App\Repositories\Mysql\OrderRepository;
use App\Repositories\Mysql\CartRepository;
use Carbon\Carbon;

class CreateOrder implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $orderNumber;

    public function __construct($orderNumber)
    {
        $this->orderNumber = $orderNumber;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(OrderRepository $orderRepository, CartRepository $cartRepository)
    {
        try {
            $dataCartSelected = $cartRepository->getSelected();
            $productData = [];
            $totalPrice = 0;
            $totalQuantity = 0;
            foreach ($dataCartSelected as $key => $value) {
                $product =  json_decode($value['data_product'], true);
                foreach ($product as $key2 => $value2) {
                    array_push($productData, $value2);
                }
                $totalQuantity = $totalQuantity + $value['total_quantity'];
                $totalPrice = $totalPrice + $value['total_price'];
            }
            $dataOrder = [
                "order_number" => $this->orderNumber,
                "checkout_date" => Carbon::now()->toDateString(),
                "product_data" => json_encode($productData),
                "total_product" => count($productData),
                "total_quantity" => $totalQuantity,
                "total_price" => $totalPrice,
                "created_at" => Carbon::now()->toDateTimeString(),
                "updated_at" => Carbon::now()->toDateTimeString(),
            ];
            $orderRepository->store($dataOrder);
        } catch (\Exception $e) {
            Log::info($e->getMessage());
        }
    }
}
