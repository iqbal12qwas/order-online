<?php

namespace App\Traits;

use Symfony\Component\HttpFoundation\Response;

trait AdminTrait
{
    protected function returnJsonSuccess($msg = [])
    {
        $result = [
            'success' => true,
            'code' => 200
        ];
        $result = array_merge($result, $msg);
        return response()->json($result, Response::HTTP_OK);
    }

    protected function returnJsonFailed($msg = [], $headCode = 400)
    {
        $result = [
            'success' => false,
            'code' => $headCode,
            'data' => (object)[],
            'message' => $msg

        ];
        return response()->json($result, $headCode);
    }

    protected function returnAcceptedFailed($msg = [], $headCode = 400)
    {
        $result = [
            'success' => false,
            'code' => $headCode,
            'data' => (object)[],
            'message' => $msg
        ];
        return response()->json($result, 202);
    }
}
