<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Services\ProductService;
use App\Services\CartService;
use App\Services\OrderService;
use App\Traits\AdminTrait;
use Illuminate\Http\Request;

class TaskController extends Controller
{
    use AdminTrait;

    protected $productService, $cartService, $orderService;

    public function __construct(ProductService $productServ, CartService $cartServ, OrderService $orderServ)
    {
        $this->productService = $productServ;
        $this->cartService = $cartServ;
        $this->orderService = $orderServ;
    }

    public function createProduct(Request $request)
    {
        $rules = [
            'name' => 'required|min:3',
            'description' => 'required|min:10',
            'price' => 'required|min:1|integer',
            'category' => 'required',
        ];

        $hasError = validate_request($request, $rules);
        if ($hasError) {
            return $this->returnAcceptedFailed($hasError, 403);
        } else {
            try {
                return $this->productService->createProduct($request);
            } catch (\Exception $e) {
                return $this->returnAcceptedFailed($e->getMessage(), 400);
            }
        }
    }

    public function getDetailProduct($id)
    {
        try {
            return $this->productService->getDetail($id);
        } catch (\Exception $e) {
            return $this->returnAcceptedFailed($e->getMessage());
        }
    }

    public function getAllProduct()
    {
        try {
            return $this->productService->getAll();
        } catch (\Exception $e) {
            return $this->returnAcceptedFailed($e->getMessage());
        }
    }

    public function getPaginateProduct(Request $request)
    {
        $rules = [
            'page' => 'required|min:1|integer',
            'per_page' => 'required|min:1|integer',
        ];

        $hasError = validate_request($request, $rules);
        if ($hasError) {
            return $this->returnAcceptedFailed($hasError, 403);
        } else {
            try {
                return $this->productService->getPaginate($request);
            } catch (\Exception $e) {
                return $this->returnAcceptedFailed($e->getMessage(), 400);
            }
        }
    }

    public function createCart(Request $request)
    {
        $rules = [
            'data' => 'required',
        ];

        $hasError = validate_request($request, $rules);
        if ($hasError) {
            return $this->returnAcceptedFailed($hasError, 403);
        } else {
            try {
                return $this->cartService->createCart($request);
            } catch (\Exception $e) {
                return $this->returnAcceptedFailed($e->getMessage(), 400);
            }
        }
    }

    public function deleteCartByIds(Request $request)
    {
        try {
            return $this->cartService->deleteCartByIds($request);
        } catch (\Exception $e) {
            return $this->returnAcceptedFailed($e->getMessage());
        }
    }

    public function selectCartByIds(Request $request)
    {
        $rules = [
            'ids' => 'required',
        ];

        $hasError = validate_request($request, $rules);
        if ($hasError) {
            return $this->returnAcceptedFailed($hasError, 403);
        } else {
            try {
                return $this->cartService->selectCartByIds($request);
            } catch (\Exception $e) {
                return $this->returnAcceptedFailed($e->getMessage(), 400);
            }
        }
    }

    public function createOrder()
    {
        try {
            return $this->orderService->createOrder();
        } catch (\Exception $e) {
            return $this->returnAcceptedFailed($e->getMessage(), 400);
        }
    }

    public function updatePaid(Request $request)
    {
        $rules = [
            'ids' => 'required',
        ];

        $hasError = validate_request($request, $rules);
        if ($hasError) {
            return $this->returnAcceptedFailed($hasError, 403);
        } else {
            try {
                return $this->orderService->updatePaid($request);
            } catch (\Exception $e) {
                return $this->returnAcceptedFailed($e->getMessage(), 400);
            }
        }
    }

    public function uploadPaymentOrder(Request $request)
    {
        $rules = [
            'id' => 'required|min:1|integer',
            'file' => 'required',
        ];

        $hasError = validate_request($request, $rules);
        if ($hasError) {
            return $this->returnAcceptedFailed($hasError, 403);
        } else {
            try {
                return $this->orderService->uploadPaymentOrder($request);
            } catch (\Exception $e) {
                return $this->returnAcceptedFailed($e->getMessage(), 400);
            }
        }
    }

    public function updateApprove(Request $request)
    {
        $rules = [
            'ids' => 'required',
            'is_approve' => 'required|digits:1|integer',
        ];

        $hasError = validate_request($request, $rules);
        if ($hasError) {
            return $this->returnAcceptedFailed($hasError, 403);
        } else {
            try {
                return $this->orderService->updateApprove($request);
            } catch (\Exception $e) {
                return $this->returnAcceptedFailed($e->getMessage(), 400);
            }
        }
    }

    public function getAllOrder()
    {
        try {
            return $this->orderService->getAll();
        } catch (\Exception $e) {
            return $this->returnAcceptedFailed($e->getMessage());
        }
    }

    public function getPaginateOrder(Request $request)
    {
        $rules = [
            'page' => 'required|min:1|integer',
            'per_page' => 'required|min:1|integer',
        ];

        $hasError = validate_request($request, $rules);
        if ($hasError) {
            return $this->returnAcceptedFailed($hasError, 403);
        } else {
            try {
                return $this->orderService->getPaginate($request);
            } catch (\Exception $e) {
                return $this->returnAcceptedFailed($e->getMessage(), 400);
            }
        }
    }

    public function getSummaryOrder()
    {
        try {
            return $this->orderService->getSummaryOrder();
        } catch (\Exception $e) {
            return $this->returnAcceptedFailed($e->getMessage());
        }
    }
}
