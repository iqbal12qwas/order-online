<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ListOrder extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "order_number" => $this->order_number,
            "checkout_date" => $this->checkout_date,
            "product_data" => json_decode($this->product_data, true),
            "total_product" => $this->total_product,
            "total_quantity" => $this->total_quantity,
            "total_price" => $this->total_price,
        ];
    }
}
