<?php

namespace App\Repositories\Mysql;

use App\Models\Mysql\Cart;

class CartRepository
{
    protected $cartModel;

    public function __construct(Cart $cart)
    {
        $this->cartModel = $cart;
    }

    public function store($data)
    {
        return $this->cartModel->create($data);
    }

    public function delete($id)
    {
        return $this->cartModel->where('id', $id)->delete();
    }

    public function update($ids, $rawData)
    {
        return $this->cartModel->whereIn('id', $ids)->update($rawData);
    }

    public function getSelected()
    {
        return $this->cartModel->selectRaw('data_product, total_quantity, total_product, total_quantity, total_price, created_at')->where('is_selected', 1)->get();
    }
}
