<?php

namespace App\Repositories\Mysql;

use App\Models\Mysql\Order;

class OrderRepository
{
    protected $orderModel;

    public function __construct(Order $order)
    {
        $this->orderModel = $order;
    }

    public function store($data)
    {
        return $this->orderModel->create($data);
    }

    public function update($ids, $rawData)
    {
        return $this->orderModel->whereIn('id', $ids)->update($rawData);
    }

    public function getAll()
    {
        return $this->orderModel->get();
    }

    public function getPeginate($limit)
    {
        return $this->orderModel->paginate($limit);
    }

    public function getSummaryNotPaid()
    {
        return $this->orderModel
            ->selectRaw('COUNT(id) as total_order, IFNULL(SUM(JSON_LENGTH(product_data)), 0) as all_product, IFNULL(SUM(total_product), 0) as total_product, IFNULL(SUM(total_quantity), 0) as total_quantity, IFNULL(SUM(total_price), 0) as total_price')
            ->whereRaw('is_paid = 0 AND is_payment_uploaded = 0 AND is_approve = 0 AND is_deleted = 0 AND deleted_at IS NULL')
            ->first();
    }

    public function getSummaryPaid()
    {
        return $this->orderModel
            ->selectRaw('COUNT(id) as total_order, IFNULL(SUM(JSON_LENGTH(product_data)), 0) as all_product, IFNULL(SUM(total_product), 0) as total_product, IFNULL(SUM(total_quantity), 0) as total_quantity, IFNULL(SUM(total_price), 0) as total_price')
            ->whereRaw('is_paid = 1 AND is_payment_uploaded = 0 AND is_approve = 0 AND is_deleted = 0 AND deleted_at IS NULL')
            ->first();
    }

    public function getSummaryPaymentUploaded()
    {
        return $this->orderModel
            ->selectRaw('COUNT(id) as total_order, IFNULL(SUM(JSON_LENGTH(product_data)), 0) as all_product, IFNULL(SUM(total_product), 0) as total_product, IFNULL(SUM(total_quantity), 0) as total_quantity, IFNULL(SUM(total_price), 0) as total_price')
            ->whereRaw('is_paid = 1 AND is_payment_uploaded = 1 AND is_approve = 0 AND is_deleted = 0 AND deleted_at IS NULL')
            ->first();
    }

    public function getSummaryApproved()
    {
        return $this->orderModel
            ->selectRaw('COUNT(id) as total_order, IFNULL(SUM(JSON_LENGTH(product_data)), 0) as all_product, IFNULL(SUM(total_product), 0) as total_product, IFNULL(SUM(total_quantity), 0) as total_quantity, IFNULL(SUM(total_price), 0) as total_price')
            ->whereRaw('is_paid = 1 AND is_payment_uploaded = 1 AND is_approve = 1 AND is_deleted = 0 AND deleted_at IS NULL')
            ->first();
    }

    public function getSummaryRejected()
    {
        return $this->orderModel
            ->selectRaw('COUNT(id) as total_order, IFNULL(SUM(JSON_LENGTH(product_data)), 0) as all_product, IFNULL(SUM(total_product), 0) as total_product, IFNULL(SUM(total_quantity), 0) as total_quantity, IFNULL(SUM(total_price), 0) as total_price')
            ->whereRaw('is_paid = 1 AND is_payment_uploaded = 1 AND is_approve = 9 AND is_deleted = 0 AND deleted_at IS NULL')
            ->first();
    }
}
