<?php

namespace App\Repositories;

use App\Models\Mongodb\Transportation;
use App\Models\Mongodb\Motorcycle;
use App\Models\Mongodb\Car;

class ReportRepository
{
    protected $transportationModel, $motorcycleModel, $carModel;

    public function __construct(Transportation $transportation, Motorcycle $motorcycle, Car $car)
    {
        $this->transportationModel = $transportation;
        $this->motorcycleModel = $motorcycle;
        $this->carModel = $car;
    }

    public function carSellingReport()
    {
        $totalSelling = $this->carModel->raw(function ($collection) {
            return $collection->aggregate([
                ['$lookup' => [
                    'from' => 'transportations',
                    'localField' => 'id_transportation',
                    'foreignField' => '_id',
                    'as' => 'transportations'
                ]],
                ['$unwind' => ['path' => '$transportations', 'preserveNullAndEmptyArrays' => true]],
                ['$project' => [
                    'price' => '$transportations.price'
                ]],
                ['$group' => [
                    '_id' => 0,
                    'total_price' => ['$sum' => '$price']
                ]]
            ]);
        });
        return $totalSelling[0];
    }

    public function motorcycleSellingReport()
    {
        $totalSelling = $this->motorcycleModel->raw(function ($collection) {
            return $collection->aggregate([
                ['$lookup' => [
                    'from' => 'transportations',
                    'localField' => 'id_transportation',
                    'foreignField' => '_id',
                    'as' => 'transportations'
                ]],
                ['$unwind' => ['path' => '$transportations', 'preserveNullAndEmptyArrays' => true]],
                ['$project' => [
                    'price' => '$transportations.price'
                ]],
                ['$group' => [
                    '_id' => 0,
                    'total_price' => ['$sum' => '$price']
                ]]
            ]);
        });
        return $totalSelling[0];
    }
}
