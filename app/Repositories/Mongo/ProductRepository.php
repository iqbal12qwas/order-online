<?php

namespace App\Repositories\Mongo;

use App\Models\Mongodb\Product;

class ProductRepository
{
    protected $productModel;

    public function __construct(Product $product)
    {
        $this->productModel = $product;
    }

    public function store($data)
    {
        return $this->productModel->create($data);
    }

    public function getAll()
    {
        return $this->productModel->get();
    }

    public function getPeginate($limit)
    {
        return $this->productModel->paginate($limit);
    }

    public function getById($id)
    {
        return $this->productModel->where('_id', $id)->first();
    }

    public function update($ids, $rawData)
    {
        return $this->productModel->whereIn('id', $ids)->update($rawData);
    }
}
