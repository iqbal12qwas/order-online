<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order', function (Blueprint $table) {
            $table->id();
            $table->string('order_number')->nullable()->change();
            $table->date('checkout_date')->nullable();
            $table->json('product_data')->nullable()->change();
            $table->integer('total_product')->unsigned()->default(0);
            $table->integer('total_quantity')->unsigned()->default(0);
            $table->integer('total_price')->unsigned()->default(0);
            $table->unsignedTinyInteger('is_paid')->default(0);
            $table->unsignedTinyInteger('is_approve')->default(0);
            $table->unsignedTinyInteger('is_payment_uploaded')->default(0);
            $table->string('payment_uploaded_file')->default('');
            $table->dateTime('payment_uploaded_file_at')->nullable();
            $table->text('notes');
            $table->unsignedTinyInteger('is_deleted')->default(0);
            $table->unsignedTinyInteger('created_by')->default(0);
            $table->unsignedTinyInteger('updated_by')->default(0);
            $table->unsignedTinyInteger('deleted_by')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order');
    }
}
