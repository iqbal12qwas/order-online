<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class GetTaskTest extends TestCase
{
    /** @test */
    public function test_get_all_product()
    {
        $response = $this->get('/api/task/product/all')
            ->assertStatus(200)
            ->assertJsonStructure(
                [
                    'success',
                    'code',
                    'data',
                ],
            );

        print_r($response->getContent());
    }

    /** @test */
    public function test_get_paginate_product()
    {
        $response = $this->get('api/task/product/paginate?page=1&per_page=10')
            ->assertStatus(200)
            ->assertJsonStructure(
                [
                    'success',
                    'code',
                    'data',
                    'meta',
                ],
            );

        print_r($response->getContent());
    }
}
