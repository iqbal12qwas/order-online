<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class PostTaskTest extends TestCase
{
    /** @test */
    public function test_create_product()
    {
        $response = $this->post('/api/task/product/create', [
            "name" => "Celana Chino Panjang Warna Biru",
            "description" => "Celana chino panjang dengan bahan yang strech",
            "price" => 140000,
            "category" => "Long Pants"
        ])
            ->assertStatus(200)
            ->assertJsonStructure(
                [
                    'success',
                    'code',
                    'data',
                ],
            );

        print_r($response->getContent());
    }
}
