<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class DeleteTaskTest extends TestCase
{
    /** @test */
    public function test_delete_cart()
    {
        $response = $this->delete('/api/task/cart/delete', [
            "id" => [6]
        ])
            ->assertStatus(200)
            ->assertJsonStructure(
                [
                    'success',
                    'code',
                ],
            );

        print_r($response->getContent());
    }
}
