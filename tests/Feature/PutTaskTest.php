<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class PutTaskTest extends TestCase
{
    /** @test */
    public function test_put_cart()
    {
        $response = $this->put('/api/task/cart/select', [
            "ids" => [3, 5]
        ])
            ->assertStatus(200)
            ->assertJsonStructure(
                [
                    'success',
                    'code',
                ],
            );

        print_r($response->getContent());
    }
}
