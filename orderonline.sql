-- MySQL dump 10.13  Distrib 8.0.33, for macos13 (arm64)
--
-- Host: 127.0.0.1    Database: orderonline
-- ------------------------------------------------------
-- Server version	8.0.33

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cart`
--

DROP TABLE IF EXISTS `cart`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cart` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `data_product` json NOT NULL,
  `total_product` int unsigned NOT NULL,
  `total_quantity` int unsigned DEFAULT NULL,
  `total_price` bigint DEFAULT NULL,
  `is_selected` int unsigned DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cart`
--

LOCK TABLES `cart` WRITE;
/*!40000 ALTER TABLE `cart` DISABLE KEYS */;
INSERT INTO `cart` VALUES (3,'[{\"_id\": \"64d4dcdd5645b4d2110e6e22\", \"name\": \"Sepatu Casual Merek Naiky\", \"price\": 5000000, \"category\": \"Casuel Shoes\", \"created_at\": \"2023-08-10T12:49:33.000000Z\", \"updated_at\": \"2023-08-10T12:49:33.000000Z\", \"description\": \"Sepatu bagus keren bahannya tidak mudah kotor\"}, {\"_id\": \"64d4dd715645b4d2110e6e23\", \"name\": \"Baju Kemeja Flanel Warna Biru\", \"price\": 120000, \"category\": \"Shirt\", \"created_at\": \"2023-08-10T12:52:01.000000Z\", \"updated_at\": \"2023-08-10T12:52:01.000000Z\", \"description\": \"Baju bagus keren bahannya adem\"}]',2,4,10240000,1,'2023-08-10 14:27:57','2023-08-11 02:46:52'),(4,'[{\"_id\": \"64d4dcdd5645b4d2110e6e22\", \"name\": \"Sepatu Casual Merek Naiky\", \"price\": 5000000, \"category\": \"Casuel Shoes\", \"created_at\": \"2023-08-10T12:49:33.000000Z\", \"updated_at\": \"2023-08-10T12:49:33.000000Z\", \"description\": \"Sepatu bagus keren bahannya tidak mudah kotor\"}, {\"_id\": \"64d4dd715645b4d2110e6e23\", \"name\": \"Baju Kemeja Flanel Warna Biru\", \"price\": 120000, \"category\": \"Shirt\", \"created_at\": \"2023-08-10T12:52:01.000000Z\", \"updated_at\": \"2023-08-10T12:52:01.000000Z\", \"description\": \"Baju bagus keren bahannya adem\"}]',2,6,5600000,0,'2023-08-10 14:28:02','2023-08-10 14:28:02'),(5,'[{\"_id\": \"64d4f4dc8be49c22f30ad1d2\", \"name\": \"Celana Bahan Warna Hitam\", \"price\": 230000, \"category\": \"Pants\", \"created_at\": \"2023-08-10T14:31:56.000000Z\", \"updated_at\": \"2023-08-10T14:31:56.000000Z\", \"description\": \"Celana Bahan Untuk Berangkat Ngantor\"}, {\"_id\": \"64d4f5168be49c22f30ad1d3\", \"name\": \"Kaos Lengan Pendek Warna Hitam\", \"price\": 75000, \"category\": \"T-Shirt\", \"created_at\": \"2023-08-10T14:32:54.000000Z\", \"updated_at\": \"2023-08-10T14:32:54.000000Z\", \"description\": \"Kaos lengan pendek dengan bahan yang dingin\"}]',2,6,760000,1,'2023-08-10 14:33:32','2023-08-11 02:46:52'),(7,'[{\"_id\": \"64d4dd715645b4d2110e6e23\", \"name\": \"Baju Kemeja Flanel Warna Biru\", \"price\": 120000, \"category\": \"Shirt\", \"created_at\": \"2023-08-10T12:52:01.000000Z\", \"updated_at\": \"2023-08-10T12:52:01.000000Z\", \"description\": \"Baju bagus keren bahannya adem\"}, {\"_id\": \"64d4f5168be49c22f30ad1d3\", \"name\": \"Kaos Lengan Pendek Warna Hitam\", \"price\": 75000, \"category\": \"T-Shirt\", \"created_at\": \"2023-08-10T14:32:54.000000Z\", \"updated_at\": \"2023-08-10T14:32:54.000000Z\", \"description\": \"Kaos lengan pendek dengan bahan yang dingin\"}, {\"_id\": \"64d4f4dc8be49c22f30ad1d2\", \"name\": \"Celana Bahan Warna Hitam\", \"price\": 230000, \"category\": \"Pants\", \"created_at\": \"2023-08-10T14:31:56.000000Z\", \"updated_at\": \"2023-08-10T14:31:56.000000Z\", \"description\": \"Celana Bahan Untuk Berangkat Ngantor\"}]',3,6,895000,1,'2023-08-10 23:07:12','2023-08-10 23:07:12');
/*!40000 ALTER TABLE `cart` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order`
--

DROP TABLE IF EXISTS `order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `order` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `order_number` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '',
  `checkout_date` date DEFAULT NULL,
  `product_data` json NOT NULL,
  `total_product` int unsigned NOT NULL,
  `total_quantity` int unsigned NOT NULL,
  `total_price` int unsigned NOT NULL,
  `is_paid` tinyint unsigned NOT NULL DEFAULT '0',
  `is_approve` tinyint unsigned NOT NULL DEFAULT '0',
  `is_payment_uploaded` tinyint unsigned NOT NULL DEFAULT '0',
  `payment_uploaded_file` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '',
  `payment_uploaded_file_at` datetime DEFAULT NULL,
  `notes` text CHARACTER SET latin1 COLLATE latin1_bin,
  `is_deleted` tinyint unsigned NOT NULL DEFAULT '0',
  `created_by` int unsigned NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int unsigned NOT NULL DEFAULT '0',
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_by` int unsigned NOT NULL DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `order_number` (`order_number`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order`
--

LOCK TABLES `order` WRITE;
/*!40000 ALTER TABLE `order` DISABLE KEYS */;
INSERT INTO `order` VALUES (1,'ON/1691713549/00001','2023-08-11','[{\"_id\": \"64d4dcdd5645b4d2110e6e22\", \"name\": \"Sepatu Casual Merek Naiky\", \"price\": 5000000, \"category\": \"Casuel Shoes\", \"created_at\": \"2023-08-10T12:49:33.000000Z\", \"updated_at\": \"2023-08-10T12:49:33.000000Z\", \"description\": \"Sepatu bagus keren bahannya tidak mudah kotor\"}, {\"_id\": \"64d4dd715645b4d2110e6e23\", \"name\": \"Baju Kemeja Flanel Warna Biru\", \"price\": 120000, \"category\": \"Shirt\", \"created_at\": \"2023-08-10T12:52:01.000000Z\", \"updated_at\": \"2023-08-10T12:52:01.000000Z\", \"description\": \"Baju bagus keren bahannya adem\"}, {\"_id\": \"64d4f4dc8be49c22f30ad1d2\", \"name\": \"Celana Bahan Warna Hitam\", \"price\": 230000, \"category\": \"Pants\", \"created_at\": \"2023-08-10T14:31:56.000000Z\", \"updated_at\": \"2023-08-10T14:31:56.000000Z\", \"description\": \"Celana Bahan Untuk Berangkat Ngantor\"}, {\"_id\": \"64d4f5168be49c22f30ad1d3\", \"name\": \"Kaos Lengan Pendek Warna Hitam\", \"price\": 75000, \"category\": \"T-Shirt\", \"created_at\": \"2023-08-10T14:32:54.000000Z\", \"updated_at\": \"2023-08-10T14:32:54.000000Z\", \"description\": \"Kaos lengan pendek dengan bahan yang dingin\"}, {\"_id\": \"64d4dd715645b4d2110e6e23\", \"name\": \"Baju Kemeja Flanel Warna Biru\", \"price\": 120000, \"category\": \"Shirt\", \"created_at\": \"2023-08-10T12:52:01.000000Z\", \"updated_at\": \"2023-08-10T12:52:01.000000Z\", \"description\": \"Baju bagus keren bahannya adem\"}, {\"_id\": \"64d4f5168be49c22f30ad1d3\", \"name\": \"Kaos Lengan Pendek Warna Hitam\", \"price\": 75000, \"category\": \"T-Shirt\", \"created_at\": \"2023-08-10T14:32:54.000000Z\", \"updated_at\": \"2023-08-10T14:32:54.000000Z\", \"description\": \"Kaos lengan pendek dengan bahan yang dingin\"}, {\"_id\": \"64d4f4dc8be49c22f30ad1d2\", \"name\": \"Celana Bahan Warna Hitam\", \"price\": 230000, \"category\": \"Pants\", \"created_at\": \"2023-08-10T14:31:56.000000Z\", \"updated_at\": \"2023-08-10T14:31:56.000000Z\", \"description\": \"Celana Bahan Untuk Berangkat Ngantor\"}]',7,16,11895000,1,0,1,'public/VYgTJVUuW6HIilZtbPPlQkXU0CciLLh0nENzfjKj.png','2023-08-11 02:03:42',NULL,0,0,'2023-08-11 00:25:49',0,'2023-08-11 02:10:27',0,NULL),(2,'ON/1691713661/00001','2023-08-11','[{\"_id\": \"64d4dcdd5645b4d2110e6e22\", \"name\": \"Sepatu Casual Merek Naiky\", \"price\": 5000000, \"category\": \"Casuel Shoes\", \"created_at\": \"2023-08-10T12:49:33.000000Z\", \"updated_at\": \"2023-08-10T12:49:33.000000Z\", \"description\": \"Sepatu bagus keren bahannya tidak mudah kotor\"}, {\"_id\": \"64d4dd715645b4d2110e6e23\", \"name\": \"Baju Kemeja Flanel Warna Biru\", \"price\": 120000, \"category\": \"Shirt\", \"created_at\": \"2023-08-10T12:52:01.000000Z\", \"updated_at\": \"2023-08-10T12:52:01.000000Z\", \"description\": \"Baju bagus keren bahannya adem\"}, {\"_id\": \"64d4f4dc8be49c22f30ad1d2\", \"name\": \"Celana Bahan Warna Hitam\", \"price\": 230000, \"category\": \"Pants\", \"created_at\": \"2023-08-10T14:31:56.000000Z\", \"updated_at\": \"2023-08-10T14:31:56.000000Z\", \"description\": \"Celana Bahan Untuk Berangkat Ngantor\"}, {\"_id\": \"64d4f5168be49c22f30ad1d3\", \"name\": \"Kaos Lengan Pendek Warna Hitam\", \"price\": 75000, \"category\": \"T-Shirt\", \"created_at\": \"2023-08-10T14:32:54.000000Z\", \"updated_at\": \"2023-08-10T14:32:54.000000Z\", \"description\": \"Kaos lengan pendek dengan bahan yang dingin\"}, {\"_id\": \"64d4dd715645b4d2110e6e23\", \"name\": \"Baju Kemeja Flanel Warna Biru\", \"price\": 120000, \"category\": \"Shirt\", \"created_at\": \"2023-08-10T12:52:01.000000Z\", \"updated_at\": \"2023-08-10T12:52:01.000000Z\", \"description\": \"Baju bagus keren bahannya adem\"}, {\"_id\": \"64d4f5168be49c22f30ad1d3\", \"name\": \"Kaos Lengan Pendek Warna Hitam\", \"price\": 75000, \"category\": \"T-Shirt\", \"created_at\": \"2023-08-10T14:32:54.000000Z\", \"updated_at\": \"2023-08-10T14:32:54.000000Z\", \"description\": \"Kaos lengan pendek dengan bahan yang dingin\"}, {\"_id\": \"64d4f4dc8be49c22f30ad1d2\", \"name\": \"Celana Bahan Warna Hitam\", \"price\": 230000, \"category\": \"Pants\", \"created_at\": \"2023-08-10T14:31:56.000000Z\", \"updated_at\": \"2023-08-10T14:31:56.000000Z\", \"description\": \"Celana Bahan Untuk Berangkat Ngantor\"}]',7,16,11895000,1,0,0,'',NULL,NULL,0,0,'2023-08-11 00:27:41',0,'2023-08-11 08:38:51',0,NULL),(3,'ON/1691714606/00001','2023-08-11','[{\"_id\": \"64d4dcdd5645b4d2110e6e22\", \"name\": \"Sepatu Casual Merek Naiky\", \"price\": 5000000, \"category\": \"Casuel Shoes\", \"created_at\": \"2023-08-10T12:49:33.000000Z\", \"updated_at\": \"2023-08-10T12:49:33.000000Z\", \"description\": \"Sepatu bagus keren bahannya tidak mudah kotor\"}, {\"_id\": \"64d4dd715645b4d2110e6e23\", \"name\": \"Baju Kemeja Flanel Warna Biru\", \"price\": 120000, \"category\": \"Shirt\", \"created_at\": \"2023-08-10T12:52:01.000000Z\", \"updated_at\": \"2023-08-10T12:52:01.000000Z\", \"description\": \"Baju bagus keren bahannya adem\"}, {\"_id\": \"64d4f4dc8be49c22f30ad1d2\", \"name\": \"Celana Bahan Warna Hitam\", \"price\": 230000, \"category\": \"Pants\", \"created_at\": \"2023-08-10T14:31:56.000000Z\", \"updated_at\": \"2023-08-10T14:31:56.000000Z\", \"description\": \"Celana Bahan Untuk Berangkat Ngantor\"}, {\"_id\": \"64d4f5168be49c22f30ad1d3\", \"name\": \"Kaos Lengan Pendek Warna Hitam\", \"price\": 75000, \"category\": \"T-Shirt\", \"created_at\": \"2023-08-10T14:32:54.000000Z\", \"updated_at\": \"2023-08-10T14:32:54.000000Z\", \"description\": \"Kaos lengan pendek dengan bahan yang dingin\"}, {\"_id\": \"64d4dd715645b4d2110e6e23\", \"name\": \"Baju Kemeja Flanel Warna Biru\", \"price\": 120000, \"category\": \"Shirt\", \"created_at\": \"2023-08-10T12:52:01.000000Z\", \"updated_at\": \"2023-08-10T12:52:01.000000Z\", \"description\": \"Baju bagus keren bahannya adem\"}, {\"_id\": \"64d4f5168be49c22f30ad1d3\", \"name\": \"Kaos Lengan Pendek Warna Hitam\", \"price\": 75000, \"category\": \"T-Shirt\", \"created_at\": \"2023-08-10T14:32:54.000000Z\", \"updated_at\": \"2023-08-10T14:32:54.000000Z\", \"description\": \"Kaos lengan pendek dengan bahan yang dingin\"}, {\"_id\": \"64d4f4dc8be49c22f30ad1d2\", \"name\": \"Celana Bahan Warna Hitam\", \"price\": 230000, \"category\": \"Pants\", \"created_at\": \"2023-08-10T14:31:56.000000Z\", \"updated_at\": \"2023-08-10T14:31:56.000000Z\", \"description\": \"Celana Bahan Untuk Berangkat Ngantor\"}]',7,16,11895000,0,0,0,'',NULL,NULL,0,0,'2023-08-11 00:43:27',0,'2023-08-11 08:38:51',0,NULL),(4,'ON/1691714688/00001','2023-08-11','[{\"_id\": \"64d4dcdd5645b4d2110e6e22\", \"name\": \"Sepatu Casual Merek Naiky\", \"price\": 5000000, \"category\": \"Casuel Shoes\", \"created_at\": \"2023-08-10T12:49:33.000000Z\", \"updated_at\": \"2023-08-10T12:49:33.000000Z\", \"description\": \"Sepatu bagus keren bahannya tidak mudah kotor\"}, {\"_id\": \"64d4dd715645b4d2110e6e23\", \"name\": \"Baju Kemeja Flanel Warna Biru\", \"price\": 120000, \"category\": \"Shirt\", \"created_at\": \"2023-08-10T12:52:01.000000Z\", \"updated_at\": \"2023-08-10T12:52:01.000000Z\", \"description\": \"Baju bagus keren bahannya adem\"}, {\"_id\": \"64d4f4dc8be49c22f30ad1d2\", \"name\": \"Celana Bahan Warna Hitam\", \"price\": 230000, \"category\": \"Pants\", \"created_at\": \"2023-08-10T14:31:56.000000Z\", \"updated_at\": \"2023-08-10T14:31:56.000000Z\", \"description\": \"Celana Bahan Untuk Berangkat Ngantor\"}, {\"_id\": \"64d4f5168be49c22f30ad1d3\", \"name\": \"Kaos Lengan Pendek Warna Hitam\", \"price\": 75000, \"category\": \"T-Shirt\", \"created_at\": \"2023-08-10T14:32:54.000000Z\", \"updated_at\": \"2023-08-10T14:32:54.000000Z\", \"description\": \"Kaos lengan pendek dengan bahan yang dingin\"}, {\"_id\": \"64d4dd715645b4d2110e6e23\", \"name\": \"Baju Kemeja Flanel Warna Biru\", \"price\": 120000, \"category\": \"Shirt\", \"created_at\": \"2023-08-10T12:52:01.000000Z\", \"updated_at\": \"2023-08-10T12:52:01.000000Z\", \"description\": \"Baju bagus keren bahannya adem\"}, {\"_id\": \"64d4f5168be49c22f30ad1d3\", \"name\": \"Kaos Lengan Pendek Warna Hitam\", \"price\": 75000, \"category\": \"T-Shirt\", \"created_at\": \"2023-08-10T14:32:54.000000Z\", \"updated_at\": \"2023-08-10T14:32:54.000000Z\", \"description\": \"Kaos lengan pendek dengan bahan yang dingin\"}, {\"_id\": \"64d4f4dc8be49c22f30ad1d2\", \"name\": \"Celana Bahan Warna Hitam\", \"price\": 230000, \"category\": \"Pants\", \"created_at\": \"2023-08-10T14:31:56.000000Z\", \"updated_at\": \"2023-08-10T14:31:56.000000Z\", \"description\": \"Celana Bahan Untuk Berangkat Ngantor\"}]',7,16,11895000,1,1,1,'',NULL,NULL,0,0,'2023-08-11 00:44:51',0,'2023-08-11 08:38:51',0,NULL),(5,'ON/1691717231/00001','2023-08-11','[{\"_id\": \"64d4dcdd5645b4d2110e6e22\", \"name\": \"Sepatu Casual Merek Naiky\", \"price\": 5000000, \"category\": \"Casuel Shoes\", \"created_at\": \"2023-08-10T12:49:33.000000Z\", \"updated_at\": \"2023-08-10T12:49:33.000000Z\", \"description\": \"Sepatu bagus keren bahannya tidak mudah kotor\"}, {\"_id\": \"64d4dd715645b4d2110e6e23\", \"name\": \"Baju Kemeja Flanel Warna Biru\", \"price\": 120000, \"category\": \"Shirt\", \"created_at\": \"2023-08-10T12:52:01.000000Z\", \"updated_at\": \"2023-08-10T12:52:01.000000Z\", \"description\": \"Baju bagus keren bahannya adem\"}, {\"_id\": \"64d4f4dc8be49c22f30ad1d2\", \"name\": \"Celana Bahan Warna Hitam\", \"price\": 230000, \"category\": \"Pants\", \"created_at\": \"2023-08-10T14:31:56.000000Z\", \"updated_at\": \"2023-08-10T14:31:56.000000Z\", \"description\": \"Celana Bahan Untuk Berangkat Ngantor\"}, {\"_id\": \"64d4f5168be49c22f30ad1d3\", \"name\": \"Kaos Lengan Pendek Warna Hitam\", \"price\": 75000, \"category\": \"T-Shirt\", \"created_at\": \"2023-08-10T14:32:54.000000Z\", \"updated_at\": \"2023-08-10T14:32:54.000000Z\", \"description\": \"Kaos lengan pendek dengan bahan yang dingin\"}, {\"_id\": \"64d4dd715645b4d2110e6e23\", \"name\": \"Baju Kemeja Flanel Warna Biru\", \"price\": 120000, \"category\": \"Shirt\", \"created_at\": \"2023-08-10T12:52:01.000000Z\", \"updated_at\": \"2023-08-10T12:52:01.000000Z\", \"description\": \"Baju bagus keren bahannya adem\"}, {\"_id\": \"64d4f5168be49c22f30ad1d3\", \"name\": \"Kaos Lengan Pendek Warna Hitam\", \"price\": 75000, \"category\": \"T-Shirt\", \"created_at\": \"2023-08-10T14:32:54.000000Z\", \"updated_at\": \"2023-08-10T14:32:54.000000Z\", \"description\": \"Kaos lengan pendek dengan bahan yang dingin\"}, {\"_id\": \"64d4f4dc8be49c22f30ad1d2\", \"name\": \"Celana Bahan Warna Hitam\", \"price\": 230000, \"category\": \"Pants\", \"created_at\": \"2023-08-10T14:31:56.000000Z\", \"updated_at\": \"2023-08-10T14:31:56.000000Z\", \"description\": \"Celana Bahan Untuk Berangkat Ngantor\"}]',7,16,11895000,1,9,1,'',NULL,NULL,0,0,'2023-08-11 01:27:12',0,'2023-08-11 02:20:47',0,NULL),(6,'ON/1691717246/00001','2023-08-11','[{\"_id\": \"64d4dcdd5645b4d2110e6e22\", \"name\": \"Sepatu Casual Merek Naiky\", \"price\": 5000000, \"category\": \"Casuel Shoes\", \"created_at\": \"2023-08-10T12:49:33.000000Z\", \"updated_at\": \"2023-08-10T12:49:33.000000Z\", \"description\": \"Sepatu bagus keren bahannya tidak mudah kotor\"}, {\"_id\": \"64d4dd715645b4d2110e6e23\", \"name\": \"Baju Kemeja Flanel Warna Biru\", \"price\": 120000, \"category\": \"Shirt\", \"created_at\": \"2023-08-10T12:52:01.000000Z\", \"updated_at\": \"2023-08-10T12:52:01.000000Z\", \"description\": \"Baju bagus keren bahannya adem\"}, {\"_id\": \"64d4f4dc8be49c22f30ad1d2\", \"name\": \"Celana Bahan Warna Hitam\", \"price\": 230000, \"category\": \"Pants\", \"created_at\": \"2023-08-10T14:31:56.000000Z\", \"updated_at\": \"2023-08-10T14:31:56.000000Z\", \"description\": \"Celana Bahan Untuk Berangkat Ngantor\"}, {\"_id\": \"64d4f5168be49c22f30ad1d3\", \"name\": \"Kaos Lengan Pendek Warna Hitam\", \"price\": 75000, \"category\": \"T-Shirt\", \"created_at\": \"2023-08-10T14:32:54.000000Z\", \"updated_at\": \"2023-08-10T14:32:54.000000Z\", \"description\": \"Kaos lengan pendek dengan bahan yang dingin\"}, {\"_id\": \"64d4dd715645b4d2110e6e23\", \"name\": \"Baju Kemeja Flanel Warna Biru\", \"price\": 120000, \"category\": \"Shirt\", \"created_at\": \"2023-08-10T12:52:01.000000Z\", \"updated_at\": \"2023-08-10T12:52:01.000000Z\", \"description\": \"Baju bagus keren bahannya adem\"}, {\"_id\": \"64d4f5168be49c22f30ad1d3\", \"name\": \"Kaos Lengan Pendek Warna Hitam\", \"price\": 75000, \"category\": \"T-Shirt\", \"created_at\": \"2023-08-10T14:32:54.000000Z\", \"updated_at\": \"2023-08-10T14:32:54.000000Z\", \"description\": \"Kaos lengan pendek dengan bahan yang dingin\"}, {\"_id\": \"64d4f4dc8be49c22f30ad1d2\", \"name\": \"Celana Bahan Warna Hitam\", \"price\": 230000, \"category\": \"Pants\", \"created_at\": \"2023-08-10T14:31:56.000000Z\", \"updated_at\": \"2023-08-10T14:31:56.000000Z\", \"description\": \"Celana Bahan Untuk Berangkat Ngantor\"}]',7,16,11895000,1,1,1,'',NULL,NULL,0,0,'2023-08-11 01:27:27',0,'2023-08-11 08:38:51',0,NULL);
/*!40000 ALTER TABLE `order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'orderonline'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-08-11 10:05:27
