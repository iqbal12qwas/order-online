1. Install Package

`composer install`

2. Composer Autoload

`composer dump-autoload`

3. Migration DB MySQL / Imort file `orderonline.sql` with DB name `orderonline`

4. Copy `.enc.example` -> `.env`

5. Create DB Mongo DB

`orderonline`

6. Running

`php artisan serve`

7. Install & Publish Horizon

`php artisan horizon:install`

`php artisan horizon:publish`

9. Running Horizon (NB : Must be running Redis)

`php artisan horizon`

10. For Running Unit Test

`php artisan test --filter PostTest`

`php artisan test --filter GetTest`

`php artisan test --filter PutTest`

`php artisan test --filter DeleteTest`

NB : collection `Order Online.postman_collection.json`
