<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });


// Product 
Route::group([
    'namespace' => '\App\Http\Controllers\Api',
    'middleware' => ['cors'],
    'prefix' => 'task/product'
], function ($router) {
    $router->post('/create', 'TaskController@createProduct');
    $router->get('/detail/{id}', 'TaskController@getDetailProduct');
    $router->get('/all', 'TaskController@getAllProduct');
    $router->get('/paginate', 'TaskController@getPaginateProduct');
});

// Cart 
Route::group([
    'namespace' => '\App\Http\Controllers\Api',
    'middleware' => ['cors'],
    'prefix' => 'task/cart'
], function ($router) {
    $router->post('/create', 'TaskController@createCart');
    $router->delete('/delete', 'TaskController@deleteCartByIds');
    $router->put('/select', 'TaskController@selectCartByIds');
});

// Order 
Route::group([
    'namespace' => '\App\Http\Controllers\Api',
    'middleware' => ['cors'],
    'prefix' => 'task/order'
], function ($router) {
    $router->post('/create', 'TaskController@createOrder');
    $router->put('/paid', 'TaskController@updatePaid');
    $router->put('/approve', 'TaskController@updateApprove');
    $router->post('/upload-payment', 'TaskController@uploadPaymentOrder');
    $router->get('/all', 'TaskController@getAllOrder');
    $router->get('/paginate', 'TaskController@getPaginateOrder');
    $router->get('/summary', 'TaskController@getSummaryOrder');
});
